<h3>Color Selector</h3>
<div data-main-uri="<?= MPI_CS_PLUGIN_URL ?>/assets/images/court/" class="color-selector-court court">
	<img src="<?php echo MPI_CS_PLUGIN_URL ?>/assets/images/court/background.jpg">
	<div class="outside">
		<img class="active" data-color="red" src="<?php echo MPI_CS_PLUGIN_URL ?>/assets/images/court/outside/red.png" class="active">
	</div>

	<div class="inside">
		<img data-color="medium green" src="<?php echo MPI_CS_PLUGIN_URL ?>/assets/images/court/inside/mediumgreen.png" class="active">
	</div>
</div>

<div class="tabs">
	<ul class="tab_nav">
		<li class="inside active">Inside</li>
		<li class="outside">Outside</li>
	</ul>

	<div class="tab_inside active">
  <span class="colors-label">Standard</span>
  <span class="colors-label">Vibrant</span>
    <div class="color-selector-wrapper">
		  <?php include(MPI_CS_PLUGIN_DIR. 'assets/images/court/colors.svg'); ?>
    </div>
    <div class="color-selector-wrapper">
      <?php include(MPI_CS_PLUGIN_DIR. 'assets/images/court/colors2.svg'); ?>
    </div>
		<div class="color">
			<div class="new_color remove"></div>
		</div>
	</div>

	<div class="tab_outside">
  <span class="colors-label">Standard</span>
  <span class="colors-label">Vibrant</span>
    <div class="color-selector-wrapper">
      <?php include(MPI_CS_PLUGIN_DIR. 'assets/images/court/colors.svg'); ?>
    </div>
    <div class="color-selector-wrapper">
		  <?php include(MPI_CS_PLUGIN_DIR. 'assets/images/court/colors2.svg'); ?>
    </div>
		<div class="color">
			<div class="new_color remove"></div>
		</div>
	</div>

</div>
