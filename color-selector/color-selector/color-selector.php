<?php
/**
 * Plugin Name: Color Selector
 * Plugin URI:  https://mediapostinc.com
 * Description: custom SVG color selector
 * Author:      Media Post, Inc.
 * Author URI:  https://mediapostinc.com
 * Version:     1.0
 * License:     GPLv2+
 *
 */

function mpi_cs_init_script() {
	wp_register_style( 'mpi-css', plugins_url( 'assets/css/main.css', __FILE__ ));
	wp_enqueue_style( 'mpi-css' );
	wp_enqueue_script( 'mpi-color-selector-js', plugins_url('assets/js/main.js', __FILE__ ));
}
add_action('wp_enqueue_scripts', 'mpi_cs_init_script');


function mpi_cs() {
	require_once(plugin_dir_path( __FILE__ ) . 'views/main.php');
}
add_shortcode('mpi_cs', 'mpi_cs');

define('MPI_CS_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('MPI_CS_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
?>
