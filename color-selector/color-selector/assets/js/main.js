(function($) {
	$(document).ready(function() {


		var colorChange = function(inOrOut) {
			var mainUri = $('.color-selector-court').attr('data-main-uri');
			var startingColor = $('.court .'+inOrOut+' img.active').data('color');
			var tabColor = $('.tab_'+inOrOut+' .color');
			var tabColorDiv = tabColor.find('div');
			var slugColor = false;

			if (startingColor !== undefined) {
				slugColor = startingColor.replace(' ', '');
			}

			tabColorDiv.text(startingColor);
			tabColor.css("background-color", $(".tab_"+inOrOut+" .colors polygon."+slugColor+", .tab_"+inOrOut+" .colors-2 polygon."+slugColor).css("fill"));

			$(".tab_"+inOrOut+" .colors polygon, .tab_"+inOrOut+" .colors-2 polygon").on('click', function(){
				var clicked = $(this);
				var color = clicked.data('color');
				var colorSlug = color.replace(' ','');
				var fill = clicked.css('fill');
				var imageContainer = $('.color-selector-court .'+inOrOut);
				var image = imageContainer.find('img[data-color="'+color+'"]');

				if (image.length === 0) {
					imageContainer.append($(new Image()).attr('data-color', color).attr('src', mainUri+inOrOut+'/'+colorSlug+'.png'));
					image = imageContainer.find('img[data-color="'+color+'"]');
					console.log(image.length);
				}

				imageContainer.find(".active").removeClass("active");
				image.addClass('active');
				// tabColorDiv.addClass('remove');
				$('.tab_'+inOrOut+' .color .remove').remove();
				tabColor.prepend("<div class='new_color remove'>"+color+"</div>");
				tabColor.css("background-color", fill);
			});

			$('.tab_nav .'+inOrOut).on('click', function(){
				var clicked  = $(this);
				if (inOrOut === 'inside') {
					$(".tab_outside").removeClass("active");
					$(".tab_inside").addClass("active");
					$(".tab_nav .outside").removeClass("active");
					clicked.addClass("active");
				} else {
					$(".tab_inside").removeClass("active");
					$(".tab_outside").addClass("active");
					$(".tab_nav .inside").removeClass("active");
					clicked.addClass("active");
				}
			});
		};
		// $(".tab_inside .colors polygon, .tab_inside .colors-2 polygon").on("click", function(){
		// 	// $(".inside .active").removeClass("active");
		// 	// color = $(this).data("color");
		// 	// fill = $(this).css("fill");
		// 	//console.log(fill);
		// 	$('.inside img[data-color="'+color+'"]').addClass("active");
		// 	$(".tab_inside .color div").addClass("remove");
		// 	$(".tab_inside .color").prepend("<div class='new_color'>"+color+"</div>");
		// 	$(".tab_inside .color").css("background-color", fill);
		// 	$(".tab_inside .color .remove").one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
		// 		$(this).remove();
		// 		});
		// });
		colorChange('inside');
		colorChange('outside');

		// $(".tab_outside .colors polygon, .tab_outside .colors-2 polygon").on("click", function(){
		// 	$(".outside .active").removeClass("active");
		// 	color = $(this).data("color");
		// 	fill = $(this).css("fill");
		// 	//console.log(color);
		// 	$('.outside img[data-color="'+color+'"]').addClass("active");
		//
		// 	$(".tab_outside .color div").addClass("remove");
		// 	$(".tab_outside .color").prepend("<div class='new_color'>"+color+"</div>");
		// 	$(".tab_outside .color").css("background-color", fill);
		// 	$(".tab_outside .color .remove").one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
		// 		$(this).remove();
		// 		});
		// });

    // var startingInside = $(".court .inside img.active").data("color");
    // var startingOutside = $(".court .outside img.active").data("color");
    // if (startingInside !== undefined) {
    //   var slugInside = startingInside.replace(' ', '');
    // }
		//
    // if (startingOutside !== undefined) {
    //   var slugOutside = startingOutside.replace(' ', '');
    // }

		// $(".tab_inside .color div").text( startingInside );
		// $(".tab_outside .color div").text( startingOutside );

		//initially set background color for color label area on page load
		// $(".tab_inside .color").css("background-color", $(".tab_inside .colors polygon."+slugInside+", .tab_inside .colors-2 polygon."+slugInside).css("fill"));
		// $(".tab_outside .color").css("background-color", $(".tab_outside .colors polygon."+slugOutside+", .tab_outside .colors-2 polygon."+slugOutside).css("fill"));

		// $(".tab_nav .inside").on("click", function(){
		// 	$(".tab_outside").removeClass("active");
		// 	$(".tab_inside").addClass("active");
		// });
		// $(".tab_nav .outside").on("click", function(){
		// 	$(".tab_inside").removeClass("active");
		// 	$(".tab_outside").addClass("active");
		// });
		//
		// $(".tab_nav .inside").on("click", function(){
		// 	$(".tab_nav .outside").removeClass("active");
		// 	$(this).addClass("active");
		// });
		//
		// $(".tab_nav .outside").on("click", function(){
		// 	$(".tab_nav .inside").removeClass("active");
		// 	$(this).addClass("active");
		// });
	});

}(jQuery))
