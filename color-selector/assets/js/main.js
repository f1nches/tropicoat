(function($) {
	$(document).ready(function() {


		var colorChange = function(inOrOut) {
			var mainUri = $('.color-selector-court').attr('data-main-uri');
			var startingColor = $('.court .'+inOrOut+' img.active').data('color');
			var tabColor = $('.tab_'+inOrOut+' .color');
			var tabColorDiv = tabColor.find('div');
			var slugColor = false;

			if (startingColor !== undefined) {
				slugColor = startingColor.replace(' ', '');
			}

			tabColorDiv.text(startingColor);
			tabColor.css("background-color", $(".tab_"+inOrOut+" .colors polygon."+slugColor+", .tab_"+inOrOut+" .colors-2 polygon."+slugColor).css("fill"));

			$(".tab_"+inOrOut+" .colors polygon, .tab_"+inOrOut+" .colors-2 polygon").on('click', function(){
				var clicked = $(this);
				var color = clicked.data('color');
				var colorSlug = color.replace(' ','');
				var fill = clicked.css('fill');
				var imageContainer = $('.color-selector-court .'+inOrOut);
				var image = imageContainer.find('img[data-color="'+color+'"]');

				if (image.length === 0) {
					imageContainer.append($(new Image()).attr('data-color', color).attr('src', mainUri+inOrOut+'/'+colorSlug+'.png'));
					image = imageContainer.find('img[data-color="'+color+'"]');
					console.log(image.length);
				}

				imageContainer.find(".active").removeClass("active");
				image.addClass('active');
				// tabColorDiv.addClass('remove');
				$('.tab_'+inOrOut+' .color .remove').remove();
				tabColor.prepend("<div class='new_color remove'>"+color+"</div>");
				tabColor.css("background-color", fill);
			});

			$('.tab_nav .'+inOrOut).on('click', function(){
				var clicked  = $(this);
				if (inOrOut === 'inside') {
					$(".tab_outside").removeClass("active");
					$(".tab_inside").addClass("active");
					$(".tab_nav .outside").removeClass("active");
					clicked.addClass("active");
				} else {
					$(".tab_inside").removeClass("active");
					$(".tab_outside").addClass("active");
					$(".tab_nav .inside").removeClass("active");
					clicked.addClass("active");
				}
			});
		};

		colorChange('inside');
		colorChange('outside');

	});

}(jQuery))
