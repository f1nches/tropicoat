<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="./style.css">
  <link rel="stylesheet" type="text/css" href="./color-selector/assets/css/main.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="./style.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>

<body>

  <div class="main">

    <div class="row">

        <div class="col-md-6 col-md-offset-3 col-xs-12 content-container">
          <div class="logo">
            <img class="logo" src="./img/tropicoat-logo.png"/>
          </div>
          <div><?php include('./color-selector/views/main.php'); ?></div>
          <div class="contact-info">
            <p>TROPICOAT ATHLETIC SURFACE COATINGS</p>
            <p>1400 NW 13TH AVENUE</p>
            <p>POMPANO BEACH, Florida 33069</p>
            <p><a href="tel:1-800-432-2994">800-432-2994</a></p>
          </div>
        </div>

    </div>

  </div>

  <script
  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>
  <script src="./color-selector/assets/js/main.js"></script>

</body>
</html>
